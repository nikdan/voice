function [ spectrum_noise ] = noise_deduction( spectrum_noise, threshold, false_snr )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

min_value = min(spectrum_noise);
spectrum_noise = spectrum_noise - min_value;

spectrum_noise = spectrum_noise .* (threshold * sqrt(false_snr));
spectrum_noise = spectrum_noise + min_value;

end

