function [ y ] = decimation_spectrum( freq_signal, index_space )


y = zeros(size(index_space));
min_value = min(freq_signal);
freq_local = freq_signal - min_value;

for iter = 2 : (length(index_space)-1)
    index_frame = index_space(iter - 1) : index_space(iter + 1);
    frame = freq_local(index_frame);

%     disp([num2str(length(index_frame)), '  ', num2str(iter)])
%     disp('  ')
    
    if (4 < length(index_frame))
        gw = gausswin(round(length(index_frame) / 2));
        frame = filter(gw, 1, frame) ./ (round(length(index_frame)) / 4);
    end
    y(iter) = median(frame);
end
y(1) = mean(y);
y(end) = y(end-1);

y = y + min_value;

end

