

file_names = {'Levitan.mp3'; 'Clinton.mp3'; 'Eva.mp3'; 'Leo_Olga.mp3'};

% for iter = 1 : length(file_names)

[x, fs] = audioread('sample/Levitan.mp3');
% [x, fs] = audioread('sample/Clinton.mp3');
% [x, fs] = audioread('sample/Eva.mp3');
% [x, fs] = audioread('sample/Leo_Olga.mp3');
% [x, fs] = audioread('sample/Irina2.mp3');

%     [x, fs] = audioread(file_names{iter});

% x = x + (randn(size(x)) * mean(abs(x)) * 0.2);

sec_start = 0;
sec_stop = 205;
frame_stop = min(length(x), sec_stop*fs);
frame_start = max(sec_start*fs, 1);
x = mean(x(frame_start : frame_stop, :), 2);
x = x - mean(x);
x = outlier_suppression(x, fs);

band_start = 30;
[b, a] = ellip(3,3,20,band_start * 2 / fs,'high');
% freqz(b,a,fs,fs)

x = filter(b, a, x);


% sound(x, fs)

y = split_to_word(x, fs, 0.5);

% pause
% sound(y, fs)

% end

