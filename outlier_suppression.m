function [ y ] = outlier_suppression( x, fs )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


buffer_duration = 1;               % in seconds
buffer_size = round(buffer_duration * fs);

y = x;
cumm_power = 0;
amount_iteration = floor(length(y) / buffer_size);
for iter = 1 : amount_iteration
    
    start_index = max(round((iter - 1) * buffer_size), 1);
    end_index = min(start_index + buffer_size, length(x));
    
    buffer = y(start_index : end_index);
    power = norm(buffer);
    
    if ((amount_iteration/3 < iter) && ((10*cumm_power/iter) < power))
        buffer = buffer .* (1 - chebwin(end_index - start_index + 1));
        y(start_index : end_index) = buffer;
        power = norm(power);
    end
    
    cumm_power = cumm_power + power;
end


end

