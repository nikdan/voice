

inner_male_female_array = zeros(1, length(file_names));
for file_iter = 1 : length(file_names)
    
    [x, fs] = audioread(strcat('sample/', file_names{file_iter}));
        
    sec_start = 0;
    sec_stop = floor(length(x) * 5/6 / fs) + 50000000;
    
    frame_stop = min(length(x), sec_stop*fs);
    frame_start = max(sec_start*fs, 1);
    x = mean(x(frame_start : frame_stop, :), 2);
    x = x - mean(x);
    x = outlier_suppression(x, fs);
    
    band_start = 30;
    [b, a] = ellip(3,3,20,band_start * 2 / fs,'high');
    % freqz(b,a,fs,fs)
    
    x = filter(b, a, x);
    x = x ./ max(x);
    

    local_estimation = 0;
    for iter = 1 : 4
        sec_duration = 20;
        last_index = length(x) - sec_duration*fs - 1;
        start_index = randi(last_index);
        stop_index = start_index + sec_duration*fs;
        y = x(start_index : stop_index);
        human = split_to_word(y, fs, 0.1);
        
        [~, scores] = predict(cl, human);        
        local_estimation = local_estimation + scores(2);
    end
    estimation = local_estimation ./ 4;
    
    inner_male_female_array(file_iter) = estimation;    
end

